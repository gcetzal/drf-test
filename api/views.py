from rest_framework import viewsets, views

from .serializers import CourseSerializer, LessonSerializer
from .models import Course, Lesson
from django.shortcuts import get_object_or_404



# Create your views here.

class CourseViewSet(viewsets.ModelViewSet):
    queryset = Course.objects.all()
    serializer_class = CourseSerializer


class LessonViewSet(viewsets.ModelViewSet):
    queryset = Lesson.objects.all()
    serializer_class = LessonSerializer

