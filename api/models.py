from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save, post_delete


# Create your models here.
class Course(models.Model):
    name_course = models.CharField(max_length=150)
    course_approved = models.BooleanField(default=False)

    def __str__(self):
        return self.name_course


class Lesson(models.Model):
    course = models.ForeignKey(Course, on_delete=models.CASCADE, related_name="courses")
    name_lesson = models.CharField(max_length=150)
    min_score = models.IntegerField(default=0)
    lesson_approved = models.BooleanField(default=False)
